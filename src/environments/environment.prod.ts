import { AuthConfig } from "angular-oauth2-oidc";

export const environment = {
  production: true,
  search_api_url: 'https://api.spotify.com/v1/search',

  authCodeFlowConfig: <AuthConfig>{
    oidc:false,

    // Url of the Identity Provider
    // issuer: 'https://idsvr4.azurewebsites.net',
    loginUrl:'https://accounts.spotify.com/authorize',

    // URL of the SPA to redirect the user to after login
    redirectUri: window.location.origin + '/',

    // The SPA's id. The SPA is registerd with this id at the auth-server
    // clientId: 'server.code',
    clientId: '0b8e2ac0ab5a47bfa6d1ef01c19114df',

    // Just needed if your auth server demands a secret. In general, this
    // is a sign that the auth server is not configured with SPAs in mind
    // and it might not enforce further best practices vital for security
    // such applications.
    // dummyClientSecret: 'secret',
    responseType: 'token',

    // set the scope for the permissions the client should request
    // The first four are defined by OIDC.
    // Important: Request offline_access to get a refresh token
    // The api scope is a usecase specific one
    // scope: 'openid profile email offline_access apiapplication',
    scope:'playlist-modify-public playlist-modify-private playlist-read-private playlist-read-collaborative',

    showDebugInformation: true,
  }
};
