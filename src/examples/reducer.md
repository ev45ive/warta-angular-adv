```ts
[1,2,3,4].map(x => x * 2 )

[1,2,3,4].reduce((sum,x) => x + sum, 0)

[1,2,3,4].reduce((state,x) => {
    return {...state, counter: state.counter + x }
}, {
    counter: 0,
    todos: [],
})

inc = (payload=1) => ({type:'INC', payload });
dec = (payload=1) => ({type:'DEC', payload });
addTodo = (payload='newTodo') => ({type:'ADD_TODO', payload });

initialState = {
    counter: 0,
    todos: [],
}

reducer = (state = initialState,action) => {
    switch(action.type){
        case 'INC': return {
            ...state, counter: state.counter + action.payload }
        case 'DEC': return {
            ...state, counter: state.counter - action.payload }
        case 'ADD_TODO': return {
            ...state, todos:[...state.todos, action.payload ] }
        default : return state;
    }
};

state = reducer(undefined,{})
state = reducer(state,inc(10))
state = reducer(state,inc(2))
state = reducer(state,addTodo('buy milk!'))
state = reducer(state,addTodo('learn ngrx?!'))
state = reducer(state,dec('8'))



```