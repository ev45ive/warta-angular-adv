import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup, Validators, ValidatorFn, Validator, AbstractControl, ValidationErrors, AsyncValidatorFn } from '@angular/forms';
import { combineLatest, Observable } from 'rxjs';
import { debounceTime, distinctUntilChanged, filter, map, withLatestFrom } from 'rxjs/operators';

export type SearchParams = string;

@Component({
  selector: 'app-search-form',
  templateUrl: './search-form.component.html',
  styleUrls: ['./search-form.component.scss']
})
export class SearchFormComponent implements OnInit {

  @Input() set query(query: string) {
    const field = this.searchForm.get('query') as FormControl;
    const validators = field.asyncValidator;
    field.setAsyncValidators([]);
    (field).setValue(query, { emitEvent: false })
    field.setAsyncValidators(validators)
  }
  @Output() search = new EventEmitter<SearchParams>();

  censor = (badword = 'batman'): ValidatorFn =>
    (control: AbstractControl): ValidationErrors | null => {
      const isError = String(control.value).includes(badword)
      return isError ? { 'censor': { badword } } : null
    }

  asyncCensor = (badword: string): AsyncValidatorFn =>
    (control: AbstractControl): Observable<ValidationErrors | null> => {
      // this.http.post('/validate',{}).pipe(map(res =>ValidationErrors | null ))

      return new Observable((observer) => {
        // constructor
        // console.log('subscribe')

        const handle = setTimeout(() => {
          const res = this.censor(badword)(control)
          observer.next(res)
          observer.complete()
          // console.log('next i complete')
        }, 2000)

        // Teardown login / destructor
        return () => {
          // console.log('unsubscribe')
          clearTimeout(handle)
        }
      })//.subscribe({ next: console.log }).unsubscribe()
    }

  myQueryValidators = Validators.compose([
    Validators.required,
    Validators.minLength(3),
    // this.censor('batman')
  ])

  searchForm = new FormGroup({
    query: new FormControl('', this.myQueryValidators, [this.asyncCensor('batman')]),
    type: new FormControl('album')
  })

  constructor() {
    (window as any).form = this.searchForm
    const queryField = this.searchForm.get('query')!;


    const value$ = queryField.valueChanges.pipe(
      // wait for silence
      debounceTime(400),
      // longer >3 
      filter<string>(query => query.length >= 3),
      // no duplicates
      distinctUntilChanged(),
    )
    const valid$ = queryField.statusChanges.pipe(filter(s => s === 'VALID'))

    // const search$ = zip([value$, valid$]).pipe()
    // const search$ = combineLatest([value$, valid$]).pipe()
    const search$ = valid$.pipe(
      withLatestFrom(value$),
      map(([_, value]) => value)
    )

    // search$?.subscribe(console.log)
    search$?.subscribe(this.search)
    // search$.subscribe({ next: query => { this.search.next(query) } })
  }

  ngOnInit(): void {
    // this.searchForm.get('query')?.setErrors({ required: true })
    // this.searchForm.markAsTouched()
    // this.searchForm.markAllAsTouched()
    // this.searchForm.get('query')?.updateValueAndValidity()
  }

  submit() {
    this.search.emit(this.searchForm.get('query')?.value)
  }

}
