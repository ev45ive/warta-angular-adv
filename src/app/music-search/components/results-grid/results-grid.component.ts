import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';
import { SearchResult } from 'src/app/core/model/Search';

@Component({
  selector: 'app-results-grid',
  templateUrl: './results-grid.component.html',
  styleUrls: ['./results-grid.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ResultsGridComponent implements OnInit {

  @Input() results: SearchResult[] | null = []

  constructor() { }

  trackFn(index: number, item: SearchResult) {
    return item.id
  }

  ngOnInit(): void {
  }

}
