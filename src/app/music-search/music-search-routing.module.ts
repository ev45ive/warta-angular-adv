import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MusicSearchNgrxComponent } from './containers/music-search-ngrx/music-search-ngrx.component';
import { MusicSearchContainer } from './containers/music-search/music-search.container';

const routes: Routes = [
  {
    path: 'search',
    component: MusicSearchNgrxComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MusicSearchRoutingModule { }
