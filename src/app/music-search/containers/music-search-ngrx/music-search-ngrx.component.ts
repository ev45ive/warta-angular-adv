import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { searchAlbumsFailure, searchAlbumsStart, searchAlbumsSuccess } from 'src/app/actions/music-search.actions';
import { MusicSearchService } from 'src/app/core/services/music-search/music-search.service';
import { selectMusicSearchResults, selectMusicSearchState } from 'src/app/selectors/music-search.selectors';

@Component({
  selector: 'app-music-search-ngrx',
  templateUrl: './music-search-ngrx.component.html',
  styleUrls: ['./music-search-ngrx.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MusicSearchNgrxComponent implements OnInit {

  results = this.store.select(selectMusicSearchResults)
  state = this.store.select(selectMusicSearchState)

  query = this.state.pipe(select(state => state.query))
  error = this.state.pipe(select(state => state.error))
  loading = this.state.pipe(select(state => state.loading))

  searchAlbums(query: string) {
    this.store.dispatch(searchAlbumsStart(query));

    // this.service.fetchSearchResults({ type: 'album', q: query })
    //   .subscribe({
    //     next: (res) => this.store.dispatch(searchAlbumsSuccess({ data: res })),
    //     error: (error) => this.store.dispatch(searchAlbumsFailure({ error })),
    //   })
  }

  constructor(
    // private service: MusicSearchService,
    private store: Store) { }

  ngOnInit(): void {
  }

}
