import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MusicSearchNgrxComponent } from './music-search-ngrx.component';
import { Store, StoreModule } from '@ngrx/store';

describe('MusicSearchNgrxComponent', () => {
  let component: MusicSearchNgrxComponent;
  let fixture: ComponentFixture<MusicSearchNgrxComponent>;
  let store: Store;

  beforeEach(async() => {
    TestBed.configureTestingModule({
      imports: [ StoreModule.forRoot({}) ],
      declarations: [ MusicSearchNgrxComponent ]
    });

    await TestBed.compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MusicSearchNgrxComponent);
    component = fixture.componentInstance;
    store = TestBed.inject(Store);

    spyOn(store, 'dispatch').and.callThrough();
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
