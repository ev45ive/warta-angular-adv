import { ChangeDetectionStrategy, Component, Inject, OnInit } from '@angular/core';
import { ConnectableObservable, ReplaySubject, Subject, Subscription } from 'rxjs';
import { multicast, publish, publishBehavior, publishReplay, refCount, share, shareReplay, takeUntil, tap } from 'rxjs/operators';
import { Album, AlbumView, SearchResult } from 'src/app/core/model/Search';
import { MusicSearchService } from 'src/app/core/services/music-search/music-search.service';
import { SearchParams } from '../../components/search-form/search-form.component';
import { INITIAL_SEARCH_RESULTS } from '../../tokens';

@Component({
  selector: 'app-music-search',
  templateUrl: './music-search.container.html',
  styleUrls: ['./music-search.container.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MusicSearchContainer implements OnInit {
  results = this.service.resultsChange
  query = this.service.queryChange

  constructor(private service: MusicSearchService) { }

  search(queryParams: SearchParams) {
    this.service.searchAlbums(queryParams)
  }


  ngOnInit(): void {
  }


}
