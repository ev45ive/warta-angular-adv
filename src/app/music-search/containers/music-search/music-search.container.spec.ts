import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MusicSearchContainer } from './music-search.container';

describe('MusicSearchContainer', () => {
  let component: MusicSearchContainer;
  let fixture: ComponentFixture<MusicSearchContainer>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MusicSearchContainer ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MusicSearchContainer);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
