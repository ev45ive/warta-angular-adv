import { InjectionToken } from '@angular/core';
import { Album } from '../core/model/Search';

export const INITIAL_SEARCH_RESULTS = new InjectionToken<Album[]>('INITIAL_SEARCH_RESULTS');
