import { ModuleWithProviders, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MusicSearchRoutingModule } from './music-search-routing.module';
import { MusicSearchContainer } from './containers/music-search/music-search.container';
import { SearchFormComponent } from './components/search-form/search-form.component';
import { ResultsGridComponent } from './components/results-grid/results-grid.component';
import { AlbumCardComponent } from './components/album-card/album-card.component';
import { SharedModule } from '../shared/shared.module';
import { INITIAL_SEARCH_RESULTS } from './tokens';
import { StoreModule } from '@ngrx/store';
import * as fromMusicSearch from '../reducers/music-search.reducer';
import { EffectsModule } from '@ngrx/effects';
import { MusicSearchEffects } from '../effects/music-search.effects';
import { MusicSearchNgrxComponent } from './containers/music-search-ngrx/music-search-ngrx.component';

@NgModule({
  declarations: [
    MusicSearchContainer,
    SearchFormComponent,
    ResultsGridComponent,
    AlbumCardComponent,
    MusicSearchNgrxComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    MusicSearchRoutingModule,
    StoreModule.forFeature(fromMusicSearch.musicSearchFeatureKey, fromMusicSearch.reducer),
    EffectsModule.forFeature([
      MusicSearchEffects,
    ])
  ],
  providers: [
    {
      provide: INITIAL_SEARCH_RESULTS,
      useValue: []
    }
  ]
})
export class MusicSearchModule {

  // static forRoot(config: { mocks: [] }): ModuleWithProviders<MusicSearchModule> {
  //   return {
  //     ngModule: MusicSearchModule,
  //     providers: [
  //       {
  //         provide: 'INITIAL_SEARCH_RESULTS',
  //         useValue: config.mocks || []
  //       }
  //     ]
  //   }
  // }
}
