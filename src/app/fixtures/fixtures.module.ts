import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AlbumView } from '../core/model/Search';
import { INITIAL_SEARCH_RESULTS } from '../music-search/tokens';


const albumMockData: AlbumView[] = [
  { id: '123', name: 'Album 123', images: [{ url: 'https://www.placecage.com/c/300/300' }] },
  { id: '234', name: 'Album 234', images: [{ url: 'https://www.placecage.com/c/400/400' }] },
  { id: '345', name: 'Album 345', images: [{ url: 'https://www.placecage.com/c/500/500' }] },
  { id: '456', name: 'Album 456', images: [{ url: 'https://www.placecage.com/c/250/250' }] },
]


@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ],
  providers: [
    {
      provide: INITIAL_SEARCH_RESULTS,
      useValue: albumMockData
    }
  ]
})
export class FixturesModule { }
