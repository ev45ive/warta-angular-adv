import { createAction, props } from '@ngrx/store';
import { Playlist } from '../core/model/Playlist';

export const loadPlaylists = createAction(
  '[Playlist] Load Playlists'
);

export const loadPlaylistsSuccess = createAction(
  '[Playlist] Load Playlists Success',
  props<{ data: Playlist[] }>(),
  // (data: Playlist[]) => ({ data })
);


export const loadPlaylistsFailure = createAction(
  '[Playlist] Load Playlists Failure',
  props<{ error: Error }>()
);


export const selectPlaylist = createAction(
  '[Playlist] Select Playlist',
  (id?: Playlist['id']) => ({ id })
  // props<{ id: Playlist['id'] }>()
);

export const savePlaylist = createAction(
  '[Playlist] Save Playlist',
  // props<{ draft: Playlist }>()
  (draft: Playlist) => ({ draft })
);

export const updatePlaylist = createAction(
  '[Playlist] Update Playlist',
  // props<{ draft: Playlist }>()
  (draft: Playlist) => ({ draft })
);

export const createPlaylist = createAction(
  '[Playlist] Create Playlist',
  // props<{ draft: Playlist }>()
  (draft: Playlist) => ({ draft })
);

export const removePlaylist = createAction(
  '[Playlist] Remove Playlist',
  (id: Playlist['id']) => ({ id })
);

// type load = ReturnType<typeof loadPlaylistsSuccess>
