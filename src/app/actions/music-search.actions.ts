import { createAction, props } from '@ngrx/store';
import { SearchResult } from '../core/model/Search';

export const searchAlbumsStart = createAction(
  '[MusicSearch] Search Albums Start',
  (query: string) => ({ query })
);

export const searchAlbumsSuccess = createAction(
  '[MusicSearch]  Search Albums Success',
  props<{ data: SearchResult[] }>()
);

export const searchAlbumsFailure = createAction(
  '[MusicSearch]  Search Albums Failure',
  props<{ error: any }>()
);
