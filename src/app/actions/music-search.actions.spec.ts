import * as fromMusicSearch from './music-search.actions';

describe('loadMusicSearchs', () => {
  it('should return an action', () => {
    expect(fromMusicSearch.searchAlbumsStart().type).toBe('[MusicSearch] Load MusicSearchs');
  });
});
