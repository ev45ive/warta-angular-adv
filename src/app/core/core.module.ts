import { Inject, ModuleWithProviders, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SEARCH_API_URL } from './tokens';
import { HttpClient, HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { OAuthModule } from 'angular-oauth2-oidc';
import { AuthService } from './services/auth/auth.service';
import { AuthInterceptor } from './services/auth.interceptor';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    HttpClientModule,
    OAuthModule.forRoot()
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    }
    // {
    //   provide: HttpClient,
    //   useClass: MyExtraSpecialAwesomeHttpClient
    // }
    // {
    //   provide: AlbumsSearchService,
    //   useFactory(url: string, results: Album[]) {
    //     return new MusicSearchService(url, results)
    //   },
    //   deps: [SEARCH_API_URL, INITIAL_SEARCH_RESULTS]
    // },
    // {
    //   provide: AlbumsSearchService,
    //   useClass: MusicSearchService,
    //   // deps: [SEARCH_API_URL, INITIAL_SEARCH_RESULTS]
    // },
    // MusicSearchService
  ]
})
export class CoreModule {

  constructor(private auth: AuthService,
    @Inject(HTTP_INTERCEPTORS) private inter: any) {
    console.log(inter)
    this.auth.init()
  }

  static forRoot(config: {
    /**
     * Url for search api services
     */
    search_api_url: string
  }): ModuleWithProviders<CoreModule> {
    return {
      ngModule: CoreModule,
      providers: [
        {
          provide: SEARCH_API_URL,
          useValue: config.search_api_url
        }
      ]
    }
  }
}
