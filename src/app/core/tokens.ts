import { InjectionToken } from "@angular/core";

export const SEARCH_API_URL = new InjectionToken<string>('search_api_url')