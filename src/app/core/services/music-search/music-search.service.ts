import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { ErrorHandler, EventEmitter, Inject, Injectable, InjectionToken } from '@angular/core';
import { INITIAL_SEARCH_RESULTS } from 'src/app/music-search/tokens';
import { CoreModule } from '../../core.module';
import { SearchResult, Album, SearchResponse } from '../../model/Search';
import { SEARCH_API_URL } from '../../tokens';
import { AuthService } from '../auth/auth.service';
import { catchError, concatAll, concatMap, exhaust, exhaustMap, filter, map, mergeAll, mergeMap, pluck, startWith, switchAll, switchMap } from 'rxjs/operators'
import { BehaviorSubject, EMPTY, from, merge, Observable, of, pipe, ReplaySubject, Subject, throwError } from 'rxjs';
import { ObservableInput } from 'rxjs';
import { SearchParams } from 'src/app/music-search/components/search-form/search-form.component';

@Injectable({
  providedIn: 'root',
})
export class MusicSearchService {

  /* Store */
  private query = new Subject<SearchParams>()
  private results = new BehaviorSubject<SearchResult[]>(this.initial)

  /* Queries / Selectors */
  readonly queryChange = this.query.asObservable()
  readonly resultsChange = this.results.asObservable()

  constructor(
    private http: HttpClient,
    @Inject(SEARCH_API_URL) private search_api_url: string,
    @Inject(INITIAL_SEARCH_RESULTS) private initial: Album[]) {

    ; (window as any).subject = this.results

    /* Chain / Sequence of actions - Effects */
    this.query.pipe(
      map(query => ({ type: 'album', q: query })),
      switchMap(this.fetchSearchResults),
    ).subscribe(this.results)
  }

  /* Commands / Actions */
  searchAlbums(query: string) {
    this.query.next(query)
  }

  fetchSearchResults = (params: { type: string, q: string }) =>
    // Handle errors INSIDE switch map!
    this.http.get(this.search_api_url, { params }).pipe(
      filter(isSearchAlbumsResponse),
      map(res => res.albums.items),
      errorHandlerOp()
    )

}

export const errorHandlerOp = <T>() => pipe<Observable<T>, Observable<T>>(
  catchError(err => { return throwError(new Error('Unexpected error')) })
)

export class ParsingError extends Error {
  message = 'Invalid response'
}

function isSearchAlbumsResponse(res: any): res is SearchResponse {
  if (!res.albums?.items) { throw new ParsingError('Invalid response') }
  return true
}