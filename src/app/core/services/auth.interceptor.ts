import { ErrorHandler, Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpErrorResponse
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { AuthService } from './auth/auth.service';
import { catchError } from 'rxjs/operators';
import { SearchResponse } from '../model/Search';
import { ParsingError } from './music-search/music-search.service';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor(
    private auth: AuthService,
    private errorHandler: ErrorHandler) { }

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {

    const authRequest = request.clone({
      setHeaders: {
        Authorization: 'Bearer ' + this.auth.getToken()
      }
    })

    return next.handle(authRequest).pipe(
      catchError((error) => {
        // TIP:  https://angular.io/api/core/ErrorHandler
        this.errorHandler.handleError(error)

        if (error instanceof ParsingError) {
          return throwError(error) // ParsingError
        }

        if (!(error instanceof HttpErrorResponse)) {
          return throwError(new Error('Unexpected error'))
        }

        if (error.status === 401) {
          setTimeout(() => this.auth.login(), 1000)
          return throwError(new Error('Session expired.. logging you out!'))
        }

        if (isSpotifyServerError(error.error)) {
          return throwError(new Error(error.error.error.message))
        }
        return throwError(new Error('Unexpected server error'))

        // return of(this.results)
        // return from([this.results])
        // return [this.results]
        // return [ ([]), ([]) ]
        // return this.http.get...
        // return []
        // return EMPTY
      })
    )
  }
}

interface SpotifyError { error: { message: string } }

/* Type Guard */
function isSpotifyServerError(error: any): error is SpotifyError {
  return error?.error?.message
}
