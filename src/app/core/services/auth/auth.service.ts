import { Injectable } from '@angular/core';
import { OAuthService } from 'angular-oauth2-oidc';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  token = '';

  constructor(
    private oauth: OAuthService
  ) {
    this.oauth.configure(environment.authCodeFlowConfig)
  }

  async init() {
    await this.oauth.tryLogin({})

    this.token = this.oauth.getAccessToken()
  }

  login() {
    this.oauth.initLoginFlow()
  }

  logout() {
    this.oauth.logOut()
  }

  getToken() {
    return this.oauth.getAccessToken()
  }

}
