import { Image, Album } from './Search'

type MaybeNumberString = number | string | undefined;
type ThisAndThat = { this: string } & { that: number }
type AlbumId = Album['id']
type AlbumKeys = keyof Album

const objabc = { a: 1, b: 2, c: 3 }
type abc = typeof objabc

type keys = 'name' | 'id' | 'images';
const key: AlbumKeys = 'images'

const obj: ThisAndThat = { that: 123, this: '' }
const primitive: MaybeNumberString = undefined;


// export type AlbumView = {
//     id: string;
//     images: Image[];
//     name: string;
//     type: 'album';
// }

// export type PartialAlbum = {
//     readonly [key in keyof Album]?: Album[key]
// }
// export type Partial<T> = {
//     [key in keyof T]?: T[key]
// }
// export type Pick<T, K extends keyof T> = {
//     [key in K]?: T[key]
// }
const astrings: Pick<Album, 'id' | 'name' | 'images'> = {
    id: '', name: '', images: []
}

declare function parse<T>(text: string): T
declare function serialize<T>(data: T): string
function identity<T>(data: T): T { return data }
function getId<T extends { id: string }>(data: T): T['id'] { return data.id }

const x1 = identity<string>('123')
const x2 = identity('123' as string)
const x3: string = identity('123')


interface Vector { x: number, y: number, length: number }
interface Point { x: number, y: number }

let v: Vector = { x: 123, y: 123, length: 123 }
let p: Point = { x: 123, y: 123 }
p = v
// v = p


// export type AlbumView = {
//     id: Album['id'];
//     images: Album['images'];
//     name: Album['name'];
//     type: Album['type'];
// }

// interface Dictionary1<T> {
//     [k: string]: T | undefined
// }
// type Dictionary2<T> = {
//     [k: string]: T | undefined
// }
// const dict: Dictionary2<string> = {
//     alamakota: '123'
// }


// base.ts
export interface IShow {
    paramA: string
}

// plugin.ts
export interface IShow {
    extraParam: string
}

// impl/app.ts
export class Show implements IShow {
    paramA: string = '';
    extraParam: string = '';
}

const s = new Show()

interface FuncIn {
    (param: number): string
    (param: string): string
}
const func: FuncIn = (param: string | number /* | boolean */) => {
    if ('string' === typeof param) {
        return param.toLocaleLowerCase()
    } else if ('number' === typeof param) {
        return param.toPrecision()
    } else {
        // Exhausiveness Check:
        const _never: never = param
        throw new Error('Unexpected param')
    }
}
func(123)
func('123')

const typeLabel = 'album'
type typeFromLabel = typeof typeLabel
// const label2:typeFromLabel = 'asd' // Error

type typeOfResult = 'album' | 'playlist'
type StringOrNumber = string | number

// const xparam/* : string | number */ = 123 as string | number
const xparam/* : StringOrNumber */ = 123 as StringOrNumber

if ('string' === typeof xparam) {
    xparam.toLocaleLowerCase()
} else {
    xparam.toExponential()
}

interface Podcast { episodes?: string[] }

const podcast = {} as Podcast

if (podcast.episodes) podcast.episodes.length
const len1 = podcast.episodes ? podcast.episodes.length : 0
const len2 = podcast.episodes?.length
const len3 = podcast.episodes?.length ?? 0


type RespObj =
    | { album: { artist: string } }
    | { artist: { albums: string[] } }
    | { track: { duration: number } }
// | { podcast: { duration: number } }

function parseResult(result: RespObj)/* : string | number | string[]  */ {
    if ('album' in result) {
        return result.album.artist
    } else if ('artist' in result) {
        return result.artist.albums
    } else if ('track' in result) {
        return result.track.duration
    } else {
        checkExhaustiveness(result);
    }
}

function checkExhaustiveness(result: never): never {
    // const _never: never = result;
    throw new Error('Unexpected result type');
}


type RespObjType =
    | { type: 'album', artist: string }
    | { type: 'artist', albums: string[] }
    | { type: 'track', duration: number }
// | { type: 'podcast', duration: number }


type ResultLabel = {
    label: string;
};

function parseResultType(res: RespObjType): ResultLabel {
    switch (res.type) {
        case 'album': return { label: 'Album' }
        case 'artist': return { label: 'Artist' }
        case 'track': return { label: 'Track' }
        default: checkExhaustiveness(res);
    }
}