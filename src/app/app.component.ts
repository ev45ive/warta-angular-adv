import { ChangeDetectionStrategy, ChangeDetectorRef, Component } from '@angular/core';
import { AuthService } from './core/services/auth/auth.service';

@Component({
  selector: 'app-root, placki[sos=malinowy].kokosowe',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  // Detect changes on : events, inputs and cdr.detectChanges()
  // changeDetection: ChangeDetectionStrategy.OnPush
})
export class AppComponent {
  title = 'warta-angular';
  navbarOpen = false

  user = {
    name: 'Admin',
  }

  login(){
    this.auth.login()
  }

  constructor(private auth:AuthService) {

  }

}
