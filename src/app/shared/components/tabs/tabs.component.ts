import { Component, ContentChildren, forwardRef, Inject, OnInit, Optional, QueryList } from '@angular/core';
import { TabComponent, TabToken } from '../tab/tab.component';



@Component({
  selector: 'app-tabs',
  templateUrl: './tabs.component.html',
  styleUrls: ['./tabs.component.scss']
})
export class TabsComponent implements OnInit {

  // @ContentChildren(TabComponent) items = new QueryList<TabComponent>()
  @ContentChildren(TabToken) items = new QueryList<TabComponent>()

  toggle(active: TabComponent) {
    this.items.forEach((tab) => {
      tab.open = tab === active
    })
  }

  constructor(
    // @Inject(forwardRef(()=>TabsComponent))
    // @Optional() private parent: any
  ) { }

  ngOnInit(): void {

  }

  ngAfterContentInit() {
    this.items.forEach(item => {
      item.change.subscribe(open => {
        this.toggle(item)
      })
    })
  }
}
