import { Component, EventEmitter, OnInit } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';


@Component({
  template: ' ',
})
export class BaseCheckboxComponent implements OnInit, ControlValueAccessor {

  checked = false;

  changed = new EventEmitter()
  touched = new EventEmitter()

  constructor() { }

  writeValue(value: boolean): void {
    this.checked = value
  }

  registerOnChange(fn: any): void {
    this.changed.subscribe(fn)
  }

  registerOnTouched(fn: any): void {
    this.touched.subscribe(fn)
  }

  ngOnInit(): void {
  }

}

const stylesUrl = './awesome-checkbox.component.scss';

@Component({
  selector: 'app-awesome-checkbox',
  templateUrl: './awesome-checkbox.component.html',
  styleUrls: [stylesUrl],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: AwesomeCheckboxComponent,
      multi: true
    }
  ]
})
export class AwesomeCheckboxComponent extends BaseCheckboxComponent { }