import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AwesomeCheckboxComponent } from './awesome-checkbox.component';

describe('AwesomeCheckboxComponent', () => {
  let component: AwesomeCheckboxComponent;
  let fixture: ComponentFixture<AwesomeCheckboxComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AwesomeCheckboxComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AwesomeCheckboxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
