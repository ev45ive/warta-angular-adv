import { Component, EventEmitter, InjectionToken, Input, OnInit, Output } from '@angular/core';

export const TabToken = new InjectionToken('Tab token')

@Component({
  selector: 'app-tab',
  templateUrl: './tab.component.html',
  styleUrls: ['./tab.component.scss'],
  providers: [
    {
      provide: TabToken, useExisting: TabComponent
    }
  ]
})
export class TabComponent implements OnInit {

  @Input() title = ''

  @Input() open = false

  @Output() change = new EventEmitter()

  toggle() {
    // this.open = !this.open
    // this.parent.toggle(this)
    this.change.emit(this.open)
  }

  constructor(
    // private parent:TabsComponent
  ) {
    // this.parent.items.push(this)
  }

  ngOnInit(): void {
  }

}
