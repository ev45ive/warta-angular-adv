import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { YesnoPipe } from './pipes/yesno.pipe'
import { MatListModule } from '@angular/material/list'
import { MatButtonModule } from '@angular/material/button';
import { MatInput, MatInputModule } from '@angular/material/input';
import { MatFormFieldModule, MatHint, MatLabel } from '@angular/material/form-field';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatMenuModule } from '@angular/material/menu';
import { MatIconModule } from '@angular/material/icon';
import { ReactiveFormsModule } from '@angular/forms';
import { TabsComponent } from './components/tabs/tabs.component';
import { TabComponent } from './components/tab/tab.component';
import { RecentSearchesComponent } from './containers/recent-searches/recent-searches.component';
import { CounterComponent } from './containers/counter/counter.component';
import { AwesomeCheckboxComponent } from './components/awesome-checkbox/awesome-checkbox.component';

import { UiToolkitModule } from 'ui-toolkit';

@NgModule({
  declarations: [
    YesnoPipe,
    TabsComponent,
    TabComponent,
    RecentSearchesComponent,
    CounterComponent,
    AwesomeCheckboxComponent
  ],
  imports: [
    UiToolkitModule,
    ReactiveFormsModule,
    MatListModule,
    MatButtonModule,
    CommonModule,
    MatInputModule,
    MatFormFieldModule,
    MatCheckboxModule,
    MatMenuModule,
    MatIconModule,
  ],
  exports: [
    UiToolkitModule,
    ReactiveFormsModule,
    MatListModule,
    MatButtonModule,
    YesnoPipe,
    MatInputModule,
    MatFormFieldModule,
    MatCheckboxModule,
    MatMenuModule,
    MatIconModule,
    TabsComponent,
    TabComponent,
    RecentSearchesComponent,
    CounterComponent,
    AwesomeCheckboxComponent,
  ]
})
export class SharedModule { }
