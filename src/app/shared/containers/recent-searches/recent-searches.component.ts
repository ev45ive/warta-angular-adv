import { Component, OnInit } from '@angular/core';
import { MusicSearchService } from 'src/app/core/services/music-search/music-search.service';

@Component({
  selector: 'app-recent-searches',
  templateUrl: './recent-searches.component.html',
  styleUrls: ['./recent-searches.component.scss']
})
export class RecentSearchesComponent implements OnInit {

  queries: string[] = [
    'alice','bob','cat','text','test'
  ]

  constructor(
    private service: MusicSearchService
  ) { }

  search(query:string){
    
    this.service.searchAlbums(query)
  }

  ngOnInit(): void {
    this.service.queryChange.subscribe(query => {
      this.queries.push(query)
      this.queries = this.queries.slice(- 5)
    })
  }

}
