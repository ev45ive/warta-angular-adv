import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/reducers';
import { decrement, increment, reset, selectCounterValue } from 'src/app/reducers/counter.reducer';

@Component({
  selector: 'app-counter',
  templateUrl: './counter.component.html',
  styleUrls: ['./counter.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CounterComponent implements OnInit {

  counter = this.store.select(selectCounterValue)

  constructor(private store: Store<AppState>) { }

  ngOnInit(): void {
    // temporary state with some UUID
  }

  ngOnDestroy(): void {
    // destroy temporary redux statat
  }

  inc() {
    this.store.dispatch(increment(1))
  }
  dec() {
    this.store.dispatch(decrement(1))
  }
  reset() {
    this.store.dispatch(reset())
  }

}
