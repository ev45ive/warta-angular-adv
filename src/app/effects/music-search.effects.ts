import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { catchError, map, concatMap, mergeMap, switchMap } from 'rxjs/operators';
import { Observable, EMPTY, of } from 'rxjs';

import * as MusicSearchActions from '../actions/music-search.actions';
import { MusicSearchService } from '../core/services/music-search/music-search.service';
import { query } from '@angular/animations';



@Injectable()
export class MusicSearchEffects {

  searchAlbumStart$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(MusicSearchActions.searchAlbumsStart),
      switchMap((action) => {
        return this.service.fetchSearchResults({ type: 'album', q: action.query }).pipe(
          mergeMap(res => [
            MusicSearchActions.searchAlbumsSuccess({ data: res }),
            MusicSearchActions.searchAlbumsSuccess({ data: res }),
            MusicSearchActions.searchAlbumsSuccess({ data: res }),
          ]),
          catchError(error => of(MusicSearchActions.searchAlbumsFailure({ error })))
        )
      })
    )
  })

  // searchAlbumStart$ = createEffect(() => {
  //   return this.actions$.pipe(
  //     ofType(MusicSearchActions.searchAlbumsStart),
  //     switchMap((action) => {
  //       return this.service.fetchSearchResults({ type: 'album', q: action.query }).pipe(
  //         map(res => MusicSearchActions.searchAlbumsSuccess({ data: res })),
  //         catchError(error => of(MusicSearchActions.searchAlbumsFailure({ error })))
  //       )
  //     })
  //   )
  // })

  // searchAlbumStart$ = createEffect(() => {
  //   return this.actions$.pipe(
  //     ofType(MusicSearchActions.searchAlbumsStart),
  //     mergeMap(() => {
  //       return of(MusicSearchActions.searchAlbumsFailure({ error: { message: 'Ups..' } }))
  //     })
  //   )
  // })

  // loadMusicSearchs$ = createEffect(() => {
  //   return this.actions$.pipe( 

  //     ofType(MusicSearchActions.searchAlbumsStart),
  //     concatMap(() =>
  //       /** An EMPTY observable only emits completion. Replace with your own observable API request */
  //       EMPTY.pipe(
  //         map(data => MusicSearchActions.loadMusicSearchsSuccess({ data })),
  //         catchError(error => of(MusicSearchActions.loadMusicSearchsFailure({ error }))))
  //     )
  //   );
  // });

  constructor(
    private actions$: Actions,
    private service: MusicSearchService,) {
    this.actions$.subscribe(console.log)
  }

}
