import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { catchError, map, concatMap, exhaustMap, mergeMap, switchMapTo, switchMap } from 'rxjs/operators';
import { Observable, EMPTY, of } from 'rxjs';

import * as PlaylistsActions from '../actions/playlist.actions';
import { Playlist } from '../core/model/Playlist';
import { HttpClient } from '@angular/common/http';
import { PagingObject } from '../core/model/Search';

const mockPlaylists: Playlist[] = [
  {
    id: '123',
    name: 'moj playlist 123',
    public: true,
    description: 'opis 123'
  },
  {
    id: '234',
    name: 'moj playlist 234',
    public: false,
    description: 'opis 234'
  },
  {
    id: '345',
    name: 'moj playlist 345',
    public: true,
    description: 'opis 3245'
  },
]


@Injectable()
export class PlaylistsEffects {

  savePlaylist$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(PlaylistsActions.savePlaylist, PlaylistsActions.savePlaylist, PlaylistsActions.savePlaylist),
      exhaustMap((action) => {
        return this.http.put<Playlist>(`https://api.spotify.com/v1/playlists/${action.draft.id}`, {
          name: action.draft.name,
          public: action.draft.public,
          description: action.draft.description,
        }).pipe(
          switchMap(() => this.http.get<Playlist>(`https://api.spotify.com/v1/playlists/${action.draft.id}`)),
          mergeMap(res => [
            PlaylistsActions.updatePlaylist(res),
            PlaylistsActions.selectPlaylist(res.id),
            PlaylistsActions.loadPlaylists()
          ]),
          catchError(error => of(PlaylistsActions.loadPlaylistsFailure({ error })))
        )
      })
    )
  })

  loadPlaylistss$ = createEffect(() => {
    return this.actions$.pipe(

      ofType(PlaylistsActions.loadPlaylists),
      concatMap(() =>
        /** An EMPTY observable only emits completion. Replace with your own observable API request */
        this.http.get<PagingObject<Playlist>>('https://api.spotify.com/v1/me/playlists').pipe(
          map(data => data.items),
          map(data => PlaylistsActions.loadPlaylistsSuccess({ data })),
          catchError(error => of(PlaylistsActions.loadPlaylistsFailure({ error }))))
      )
    );
  });



  constructor(private actions$: Actions, private http: HttpClient) { }

}
