import { ApplicationRef, DoBootstrap, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { PlaylistsModule } from './playlists/playlists.module';
import { SharedModule } from './shared/shared.module';
import { CoreModule } from './core/core.module';
import { MusicSearchModule } from './music-search/music-search.module';
import { AlbumView } from './core/model/Search';
import { FixturesModule } from './fixtures/fixtures.module';
import { environment } from 'src/environments/environment';
import { StoreModule } from '@ngrx/store';
import { reducers, metaReducers } from './reducers';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import * as fromCounter from './reducers/counter.reducer';
import { EffectsModule } from '@ngrx/effects';
import { UiToolkitService } from 'ui-toolkit';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    //
    CoreModule.forRoot({
      search_api_url: environment.search_api_url
    }),
    StoreModule.forRoot(reducers, { metaReducers }),
    // !environment.production ? StoreDevtoolsModule.instrument({}) : [],
    EffectsModule.forRoot(),
    StoreDevtoolsModule.instrument({}),
    SharedModule,
    //
    PlaylistsModule, // StoreModule.forFeature(fromCounter.counterFeatureKey, fromCounter.reducer),
    MusicSearchModule,
    // MusicSearchModule.forRoot({
    //   mocks: albumMockData
    // }),
    //
    AppRoutingModule,
    FixturesModule,
  ],
  providers: [
    {
      provide: 'INITIAL_SEARCH_RESULTS',
      useValue: 'placki'
    }
  ]
  // bootstrap: [AppComponent]
  // bootstrap: [AppComponent/* ,HeaderComponent, SidebarComponent */]
})
export class AppModule implements DoBootstrap {

  constructor(private service:UiToolkitService){}

  ngDoBootstrap(appRef: ApplicationRef): void {
    // Server.fetchConfig...
    appRef.bootstrap(AppComponent, 'app-root')
  }
}
