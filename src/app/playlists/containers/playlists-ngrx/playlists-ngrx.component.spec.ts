import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PlaylistsNgrxComponent } from './playlists-ngrx.component';
import { Store, StoreModule } from '@ngrx/store';

describe('PlaylistsNgrxComponent', () => {
  let component: PlaylistsNgrxComponent;
  let fixture: ComponentFixture<PlaylistsNgrxComponent>;
  let store: Store;

  beforeEach(async() => {
    TestBed.configureTestingModule({
      imports: [ StoreModule.forRoot({}) ],
      declarations: [ PlaylistsNgrxComponent ]
    });

    await TestBed.compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PlaylistsNgrxComponent);
    component = fixture.componentInstance;
    store = TestBed.inject(Store);

    spyOn(store, 'dispatch').and.callThrough();
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
