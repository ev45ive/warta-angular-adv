import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { filter, map } from 'rxjs/operators';
import { loadPlaylists, loadPlaylistsSuccess, savePlaylist, selectPlaylist, updatePlaylist } from 'src/app/actions/playlist.actions';
import { Playlist } from 'src/app/core/model/Playlist';
import { selectCurrentPlaylist, selectPlaylists, selectPlaylistsError } from 'src/app/selectors/playlists.selectors';


@Component({
  selector: 'app-playlists-ngrx',
  templateUrl: './playlists-ngrx.component.html',
  styleUrls: ['./playlists-ngrx.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PlaylistsNgrxComponent implements OnInit {

  playlists = this.store.select(selectPlaylists);
  selected = this.store.select(selectCurrentPlaylist);
  error = this.store.select(selectPlaylistsError);

  mode = this.route.paramMap.pipe(map(paramMap => paramMap.get('mode')))

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private store: Store) { }

  selectPlaylist(id: Playlist['id']) {
    // this.store.dispatch(selectPlaylist(id))
    this.router.navigate(['/playlists', id, 'show'])
  }

  savePlaylist(draft: Playlist) {
    this.store.dispatch(savePlaylist(draft))
    this.router.navigate(['/playlists', draft.id, 'show'])
  }

  edit(id: Playlist['id']) {
    this.router.navigate(['/playlists', id, 'edit'])
  }

  ngOnInit(): void {
    // const playlist_id = this.route.snapshot.paramMap.get('playlist_id')
    // playlist_id && this.store.dispatch(selectPlaylist(playlist_id))

    this.route.paramMap.pipe(
      map(paramMap => paramMap.get('playlist_id')),
      // filter(isNotNull),
      // o => o
    ).subscribe(playlist_id => {
      this.store.dispatch(selectPlaylist(playlist_id || undefined))
    })

    this.store.dispatch(loadPlaylists());
  }

}

function isNotNull<T>(any: T | null | undefined): any is T {
  return any !== null && any !== undefined
}
