import { Component, OnInit } from '@angular/core';
import { Playlist } from "../../../core/model/Playlist";

type ModesEnum = 'details' | 'edit' | 'create';

@Component({
  selector: 'app-playlists',
  templateUrl: './playlists.container.html',
  styleUrls: ['./playlists.container.scss']
})
export class PlaylistsContainer implements OnInit {

  mode: ModesEnum = 'details'

  playlists: Playlist[] = [
    {
      id: '123',
      name: 'moj playlist 123',
      public: true,
      description: 'opis 123'
    },
    {
      id: '234',
      name: 'moj playlist 234',
      public: false,
      description: 'opis 234'
    },
    {
      id: '345',
      name: 'moj playlist 345',
      public: true,
      description: 'opis 3245'
    },
  ]

  selected?: Playlist

  edit() {
    this.mode = 'edit'
  }
  create() {
    this.mode = 'create'
  }

  cancel() {
    this.mode = 'details'
  }

  emptyPlaylist: Playlist = {
    id: '', name: '', public: false, description: ''
  }

  selectPlaylist(playlist_id?: Playlist['id']) {
    this.selected = this.playlists.find(p => p.id === playlist_id)
  }

  savePlaylist(draft: Playlist) {
    this.playlists = this.playlists.map(p => p.id === draft.id ? draft : p)
    this.selectPlaylist(draft.id)
  }

  addPlaylist(draft: Playlist) {
    this.playlists.push(draft)
    this.selectPlaylist(draft.id)
  }

  constructor() { }

  ngOnInit(): void {
  }

}
