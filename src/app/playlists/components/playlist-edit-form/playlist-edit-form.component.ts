import { Component, Directive, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, NG_VALIDATORS, ValidationErrors, Validator } from '@angular/forms';
import { Playlist } from 'src/app/core/model/Playlist';
import { PlaylistsContainer } from '../../containers/playlists/playlists.container';

@Directive({
  selector: '[customValidator]',
  providers: [{
    provide: NG_VALIDATORS, useExisting: CustomValidatorDirective, multi: true
  }]
})
export class CustomValidatorDirective implements Validator {
  validate(control: AbstractControl): ValidationErrors | null {
    return { 'custom': true };
  }
}

@Component({
  selector: 'app-playlist-edit-form',
  templateUrl: './playlist-edit-form.component.html',
  styleUrls: ['./playlist-edit-form.component.scss']
})
export class PlaylistEditFormComponent implements OnInit {
  private _originalPlaylist!: Playlist

  @Input() set playlist(playlist: Playlist) {
    this._originalPlaylist = playlist;

    this.playlistForm.setValue({
      name: playlist.name,
      public: playlist.public,
      description: playlist.description,
    })
  }

  @Output() cancel = new EventEmitter()
  @Output() save = new EventEmitter<Playlist>()

  submit() {
    this.save.emit({
      ...this._originalPlaylist, ...this.playlistForm.value
    })
    // this.parent.savePlaylist({
    //   ...this._originalPlaylist, ...this.playlistForm.value
    // })
  }

  playlistForm = this.fb.group({
    name: [''],
    public: [true],
    description: [''],
  })

  constructor(
    private fb: FormBuilder,
    // private parent: PlaylistsContainer
  ) {
    // this.parent.edit()
  }


  ngOnInit(): void {
  }

}
