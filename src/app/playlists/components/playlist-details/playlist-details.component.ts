import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Playlist } from 'src/app/core/model/Playlist';
import { PlaylistsContainer } from '../../containers/playlists/playlists.container';

@Component({
  selector: 'app-playlist-details',
  templateUrl: './playlist-details.component.html',
  styleUrls: ['./playlist-details.component.scss']
})
export class PlaylistDetailsComponent implements OnInit {

  @Input() playlist!: Playlist

  @Output() edit = new EventEmitter()

  ngOnInit(): void {
    if (!this.playlist) {
      throw new Error('Playlist Input is required')
    }
  }

}
