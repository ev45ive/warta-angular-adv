import { NgForOf, NgForOfContext } from '@angular/common';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Playlist } from '../../../core/model/Playlist';


@Component({
  selector: 'app-playlist-list',
  templateUrl: './playlist-list.component.html',
  styleUrls: ['./playlist-list.component.scss'],
  // inputs:['playlists:value']
})
export class PlaylistListComponent implements OnInit {

  // @Input('items') playlists: Playlist[] = []
  @Input() playlists: Playlist[] | null = []

  @Output() selectedChange = new EventEmitter<Playlist['id']>()

  @Input()
  selected?: string

  select(id: Playlist['id']) {
    this.selectedChange.emit(id)
    // this.selected = id
  }

  constructor() { }

  ngOnInit(): void {
  }

}
