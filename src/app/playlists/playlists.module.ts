import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PlaylistsRoutingModule } from './playlists-routing.module';
import { PlaylistsContainer } from './containers/playlists/playlists.container';
import { PlaylistListComponent } from './components/playlist-list/playlist-list.component';
import { PlaylistListItemComponent } from './components/playlist-list-item/playlist-list-item.component';
import { PlaylistDetailsComponent } from './components/playlist-details/playlist-details.component';
import { CustomValidatorDirective, PlaylistEditFormComponent } from './components/playlist-edit-form/playlist-edit-form.component';
import { SharedModule } from '../shared/shared.module';
import { StoreModule } from '@ngrx/store';
import * as fromPlaylists from '../reducers/playlists.reducer';
import { PlaylistsNgrxComponent } from './containers/playlists-ngrx/playlists-ngrx.component';
import { EffectsModule } from '@ngrx/effects';
import { PlaylistsEffects } from '../effects/playlists.effects';


@NgModule({
  declarations: [
    PlaylistsContainer,
    PlaylistListComponent,
    PlaylistListItemComponent,
    PlaylistDetailsComponent,
    PlaylistEditFormComponent,
    CustomValidatorDirective,
    PlaylistsNgrxComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    PlaylistsRoutingModule,
    StoreModule.forFeature(
      fromPlaylists.playlistsFeatureKey,
      fromPlaylists.reducer
    ),
    EffectsModule.forFeature([PlaylistsEffects]),
  ],
  exports: [
    // PlaylistsContainer
  ]
})
export class PlaylistsModule { }
