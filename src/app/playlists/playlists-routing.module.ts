import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PlaylistsNgrxComponent } from './containers/playlists-ngrx/playlists-ngrx.component';
import { PlaylistsContainer } from './containers/playlists/playlists.container';

const routes: Routes = [
  {
    path: 'playlists/:playlist_id/:mode',
    pathMatch: 'full',
    component: PlaylistsNgrxComponent
  },
  {
    path: 'playlists',
    pathMatch: 'full',
    component: PlaylistsNgrxComponent
  },
  {
    path: 'playlists_old',
    component: PlaylistsContainer
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PlaylistsRoutingModule { }
