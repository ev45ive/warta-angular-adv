import { Action } from '@ngrx/store';
import { ActionCreator, Creator, TypedAction } from '@ngrx/store/src/models';
import { AppState } from '.';

export const counterFeatureKey = 'counter';

export interface CounterState {
  currentValue: number,
  data: {
    info: {
      costam: number
    }
  }
}

export const initialState: CounterState = {
  currentValue: 0,
  data: { info: { costam: 1 } }
};

export function reducer(state = initialState, anyaction: Action): CounterState {
  const action = anyaction as Actions

  switch (action.type) {
    case 'INC': return { ...state, currentValue: state.currentValue + action.payload }
    case 'DEC': return { ...state, currentValue: state.currentValue - action.payload }
    case 'RESET': return { ...state, currentValue: 0 }
    // case 'Costam': return { ...state, data: { ...state.data, info: { ...state.data.info, costam: 2 } } }
    default:
      return state;
  }
}

/* Action Creators */
export const increment: Creator<[number], INC> = (payload = 1) => ({
  type: 'INC', payload
})
export const decrement: Creator<[number], DEC> = (payload = 1) => ({
  type: 'DEC', payload
})
export const reset: Creator<[], RESET> = () => ({
  type: 'RESET',
})

/* Selector */
export const selectCounterValue = (state: AppState) => {
  return state[counterFeatureKey].currentValue
}

/* TYPES  */
type Actions =
  | INC
  | DEC
  | RESET

interface RESET extends TypedAction<'RESET'> { }
interface INC extends TypedAction<'INC'> {
  payload: number
}

interface DEC extends TypedAction<'DEC'> {
  payload: number
}