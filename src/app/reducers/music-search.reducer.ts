import { Action, createReducer, on } from '@ngrx/store';
import * as MusicSearchActions from '../actions/music-search.actions';
import { SearchResult } from '../core/model/Search';

export const musicSearchFeatureKey = 'musicSearch';

export interface State {
  query: string
  loading: boolean
  error?: string
  results: SearchResult[]
}

export const initialState: State = {
  loading: false,
  query: '',
  results: []
};

export const reducer = createReducer(
  initialState,

  on(MusicSearchActions.searchAlbumsStart, (state, action) => ({
    ...state,
    loading: true, error: '', query: action.query
  })),
  on(MusicSearchActions.searchAlbumsSuccess, (state, action) => ({
    ...state,
    loading: false, results: action.data
  })),
  on(MusicSearchActions.searchAlbumsFailure, (state, action) => ({
    ...state,
    loading: false, error: action.error?.message
  })),

);

