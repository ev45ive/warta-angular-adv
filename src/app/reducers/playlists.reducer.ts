import { Action, createReducer, on } from '@ngrx/store';
import { Playlist } from '../core/model/Playlist';
import * as PlaylistsActions from '../actions/playlist.actions';

export const playlistsFeatureKey = 'playlists';

export interface State {
  items: Playlist[]
  loading: boolean
  error?: string
  selectedId?: Playlist['id']
}

export const initialState: State = {
  items: [],
  loading: false
};


export const reducer = createReducer(initialState,
  on(PlaylistsActions.loadPlaylists, state => ({
    ...state, loading: true, error: ''
  })),
  on(PlaylistsActions.loadPlaylistsSuccess, (state, action) => ({
    ...state,
    items: action.data,
    loading: false
  })),
  on(PlaylistsActions.loadPlaylistsFailure, (state, action) => ({
    ...state,
    error: action.error?.message,
    loading: false
  })),
  on(PlaylistsActions.selectPlaylist, (state, action) => ({
    ...state,
    selectedId: action.id
  })),
  on(PlaylistsActions.updatePlaylist, (state, action): State => ({
    ...state,
    items: state.items.map(p => p.id === action.draft.id ? action.draft : p)
  })),
  on(PlaylistsActions.removePlaylist, (state, action) => ({
    ...state,
    item: state.items.filter(p => p.id !== action.id)
  })),
  on(PlaylistsActions.createPlaylist, (state, action) => ({
    ...state,
    item: [...state.items, action.draft]
  }))

);

