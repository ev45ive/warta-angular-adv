import { createFeatureSelector, createSelector, createSelectorFactory } from '@ngrx/store';
import { playlistsFeatureKey, State } from '../reducers/playlists.reducer';


const playlistsFeature = createFeatureSelector<State>(playlistsFeatureKey)


export const selectPlaylists = createSelector(
    playlistsFeature,
    (state) => state.items
)

export const selectCurrentPlaylistId = createSelector(
    playlistsFeature,
    (state) => state.selectedId
)
export const selectPlaylistsError = createSelector(
    playlistsFeature,
    (state) => state.error
)

export const selectCurrentPlaylist = createSelector(
    selectPlaylists,
    selectCurrentPlaylistId,
    (playlists, id) => playlists.find(p => p.id === id)
)
