import * as fromMusicSearch from '../reducers/music-search.reducer';
import { selectMusicSearchState } from './music-search.selectors';

describe('MusicSearch Selectors', () => {
  it('should select the feature state', () => {
    const result = selectMusicSearchState({
      [fromMusicSearch.musicSearchFeatureKey]: {}
    });

    expect(result).toEqual({});
  });
});
