import { createFeatureSelector, createSelector } from '@ngrx/store';
import * as fromMusicSearch from '../reducers/music-search.reducer';

export const selectMusicSearchState = createFeatureSelector<fromMusicSearch.State>(
  fromMusicSearch.musicSearchFeatureKey
);

export const selectMusicSearchStatus = createSelector(
  selectMusicSearchState, state => state
)

export const selectMusicSearchResults = createSelector(
  selectMusicSearchState, state => state.results
)