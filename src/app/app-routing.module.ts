import { Route } from '@angular/compiler/src/core';
import { Inject, NgModule } from '@angular/core';
import { RouterModule, ROUTES, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: '/search'
  },
  {
    path: '**',
    pathMatch: 'full',
    redirectTo: '/search'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    useHash: true
  })],
  exports: [RouterModule],

})
export class AppRoutingModule {
  // constructor(@Inject(ROUTES) routes: Route[][]) {
  //   console.log(routes)
  // }
}
