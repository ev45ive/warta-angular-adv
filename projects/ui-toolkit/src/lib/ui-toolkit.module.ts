import { NgModule } from '@angular/core';
import { UiToolkitComponent } from './ui-toolkit.component';



@NgModule({
  declarations: [
    UiToolkitComponent
  ],
  imports: [
  ],
  exports: [
    UiToolkitComponent
  ]
})
export class UiToolkitModule { }
