import { TestBed } from '@angular/core/testing';

import { UiToolkitService } from './ui-toolkit.service';

describe('UiToolkitService', () => {
  let service: UiToolkitService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(UiToolkitService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
