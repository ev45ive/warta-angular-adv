# GIt 
cd ..
git clone https://bitbucket.org/ev45ive/warta-angular-adv.git warta-angular-adv
cd warta-angular-adv
npm i 
npm run start

# INstalacje

https://nodejs.org/en/
node -v 
v14.16.1

npm -v 
6.14.6

git --version
git version 2.31.1.windows.1

chrome --version

code -v 

## Es6
http://kangax.github.io/compat-table/es2016plus/
https://caniuse.com/?search=flatMap
https://angular.io/guide/deployment#differential-loading

# Angular CLI
~/.npmconfig

## Local
npm install @angular/cli@12.0.0
node ./node_modules/@angular/cli/ng --version

## Global
npm install -g @angular/cli@12.0.0
npx @angular/cli@12.0.0
npx @angular/cli@12.0.0 --help
npx @angular/cli@12.0.0 new ...

C:\Users\PC\AppData\Roaming\npm\ng -> C:\Users\PC\AppData\Roaming\npm\node_modules\@angular\cli\bin\ng
ng --version

Angular CLI: 12.0.0
Node: 14.16.1
Package Manager: npm 6.14.6
OS: win32 x64

node ./node_modules/@angular/cli/ng help
ng help
ng new --help

ng new warta-angular --directory=. --strict --routing --style=scss --package-manager=npm

## Linter
ng lint

Cannot find "lint" target for the specified project.

You should add a package that implements linting capabilities.

For example:
  ng add @angular-eslint/schematics


## IntelJ 
https://plugins.jetbrains.com/plugin/7450-emmet-everywhere

# Boostrap
npm i bootstrap

# Angular material
ng add @angular/material

ℹ Using package manager: npm
✔ Found compatible package version: @angular/material@12.0.0.
✔ Package information loaded.

The package @angular/material@12.0.0 will be installed and executed.
Would you like to proceed? Yes
✔ Package successfully installed.
? Choose a prebuilt theme name, or "custom" for a custom theme: 
    Indigo/Pink [ Preview: https://material.angular.io?theme=indigo-pink ]

? Set up global Angular Material typography styles? Yes 
? Set up browser animations for Angular Material? Yes
UPDATE package.json (1168 bytes)
✔ Packages installed successfully.
UPDATE src/app/app.module.ts (502 bytes)
UPDATE angular.json (3457 bytes)  
UPDATE src/index.html (580 bytes) 
UPDATE src/styles.scss (528 bytes)

## Generators / Schematics
ng g 
ng g m --help

L.I.F.T

## Playlists module
ng g m playlists --routing -m app
ng g c playlists/containers/playlists --type container

ng g c playlists/components/playlist-list
ng g c playlists/components/playlist-list-item
ng g c playlists/components/playlist-details
ng g c playlists/components/playlist-edit-form

## Shared module
ng g m shared -m app

## Pipes
ng g p shared/pipes/yesno --export

## Tabs - component communication

ng g c shared/components/tabs --export
ng g c shared/components/tab --export

## Music Search module 
ng g m music-search --routing -m app
ng g c music-search/containers/music-search --type container

ng g c music-search/components/search-form
ng g c music-search/components/results-grid
ng g c music-search/components/album-card


## Services
ng g s core/services/music-search --flat false


## Auth
npm i angular-oauth2-oidc --save

ng g s core/services/auth


## RxjS
https://rxjs.dev/api
https://rxviz.com/

https://indepth.dev/posts/1171/angular-viewmodel-as-observable
https://www.learnrxjs.io/learn-rxjs/operators
https://rxmarbles.com/#map


## Interceptors
ng g interceptor core/services/auth


## Reactive services
ng g c shared/containers/recent-searches --export


## NgRX
### ng add @ngrx/schematics
ℹ Using package manager: npm
✔ Found compatible package version: @ngrx/schematics@12.0.0.

The package @ngrx/schematics@12.0.0 will be installed and executed.
Would you like to proceed? Yes
✔ Package successfully installed.
"SchematicsNgRxSchematics" schema is using the keyword "id" which its support is deprecated. Use "$id" for schema ID.
? Do you want to use @ngrx/schematics as the default collection? Yes
UPDATE angular.json (3577 bytes)

ng g 
store
action
reducer
effect
selector

### ng add @ngrx/store
ℹ Using package manager: npm
✔ Found compatible package version: @ngrx/store@12.0.0.
✔ Package information loaded.

The package @ngrx/store@12.0.0 will be installed and executed.
Would you like to proceed? Yes
✔ Package successfully installed.
"SchematicsNgRxRootState" schema is using the keyword "id" which its support is deprecated. Use "$id" for schema ID.
UPDATE src/app/app.module.ts (1586 bytes)
UPDATE package.json (1271 bytes)
✔ Packages installed successfully.


## Generators
ng generate @ngrx/schematics:store --name=State --module=app --no-flat --root --stateInterface=AppState

ng generate @ngrx/schematics:reducer --name=counter --module=app --group --reducers=reducers/index.ts

ng generate @ngrx/schematics:reducer --name=counter --group --reducers=reducers/index.ts
  ? Should we add success and failure actions to the reducer? No
  ? Do you want to use the create function? No
  CREATE src/app/reducers/counter.reducer.spec.ts (332 bytes)
  CREATE src/app/reducers/counter.reducer.ts (292 bytes)
  UPDATE src/app/reducers/index.ts (529 bytes)
  UPDATE src/app/app.module.ts (1927 bytes)

ng generate @ngrx/schematics:container --name=shared/containers/counter --module=shared --changeDetection=OnPush --stateInterface=AppState

## Playlists Redux Feature

### Store Module 
ng generate @ngrx/schematics:reducer playlist --module=playlists --api --group --creators --feature 

### Actions
ng generate @ngrx/schematics:action --name=playlist --api --group --creators 

### Memoized Selectors
ng generate @ngrx/schematics:selector --name=playlists --group

### Container component
ng generate @ngrx/schematics:container --name=playlists/containers/playlists-ngrx --module=playlists --changeDetection=OnPush --stateInterface=AppState

## Music Search redux feature
ng generate @ngrx/schematics:feature --name=music-search --api --group --creators -m music-search

ng generate @ngrx/schematics:container --name=music-search/containers/music-search-ngrx --module=music-search --changeDetection=OnPush --stateInterface=AppState


### Custom value accessor
ng g c shared/components/awesome-checkbox --export

## Library

ng g library ui-toolkit

npm run build ui-toolkit

Building Angular Package

------------------------------------------------------------------------------
Building entry point 'ui-toolkit'
------------------------------------------------------------------------------
✔ Compiling TypeScript sources through NGC
✔ Bundling to FESM2015
✔ Bundling to UMD
✔ Writing package metadata
✔ Built ui-toolkit

------------------------------------------------------------------------------
Built Angular Package
 - from: C:\Projects\szkolenia\warta-angular-adv\projects\ui-toolkit        
 - to:   C:\Projects\szkolenia\warta-angular-adv\dist\ui-toolkit
------------------------------------------------------------------------------